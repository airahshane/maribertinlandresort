<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'maribertinlandresort' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '6qMU{*$79RN-Xb&X~y-3LvueY5#-ZYwhT16X+43(SE{s;[fgN<a%NJxF2PK0CAn{' );
define( 'SECURE_AUTH_KEY',  'l#**Y|>XTxFEjd+-Kc]iSE#xeiJbB;UiR#Eo-CYS5yOVJw?O&@Y9>%EUX/;i:=Us' );
define( 'LOGGED_IN_KEY',    ';*&;5-SD*^:%w:TWN9.)aldszzetl1gV$zTzmsl~9;$m$BqMj)[*|TT<g]v~<yAI' );
define( 'NONCE_KEY',        'AQ!-%<#.*(+S?|jlh8?Ou+BKTe1ved0,MU<P61=e}jwV?2i3zb?r/fj<xD({:oo}' );
define( 'AUTH_SALT',        '>@W!l}0`e!Rv-2zk-)@W5XFG6&ZJXM0w`A.VK|JAN#G{e_8sh$-Y&h4Gcqvljk]?' );
define( 'SECURE_AUTH_SALT', 'MQM7Q<=aGz]9 u68lT2_jzfTxWiE_mnsBKrkYK|=:=#VkUMHu]<Q,4y:^#KQZdyq' );
define( 'LOGGED_IN_SALT',   '4Tz/J{srlvUYU+oR%GjC<lQ>UPqyr^bG}$>u_h-.^z=NfAMZ|Vp(m)VBw)K< MUq' );
define( 'NONCE_SALT',       '~o_d74Ruqxw{Cu];]f=ipqTv.|m?sBA^ZIs{Jt=:]9K_[@XD{v.H.:wehO|mp9F5' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
